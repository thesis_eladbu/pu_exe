{
    "input_params": [
        {
            "name": "simulation_type",
            "value": "pure_dynamics",
            "desc": ["The type of simulation to run. Can be one of the following:",
                    "(1) pure_dynamics - running one cycle of the Solver() to depict the death kinematics per deformation applied.",
                    "(2) apoptosis_screening - running multiple simulations as a function of apoptosis threshold.",
                    "(3) sim_to_exp - running a simulation for a specific deformation profile as dictated by an indentor. The purpose here is to reconstruct the sigmoid death curve from experiments."],
            "type": "string"
        }, {
            "name": "max_sim_time",
            "value": 150,
            "desc": "The maximal time value, until which simulation is run.",
            "type": "integer"
        }, {
            "name": "mesh_size",
            "value": [19, 19],
            "desc": "The size of the mesh, i.e. tissue, given in number of cells for each dimension.",
            "type": "list"
        }, {
            "name": "include_all_mechanisms",
            "value": true,
            "desc": "Whether to include all the mechanisms defined in the dynamic model.",
            "type": "bool"
        }, {
            "name": "excluded_mechanisms",
            "value": ["apoptosis_signaling"],
            "desc": "A list of mechanisms to exclude from the simulations, given the `include_all_mechanisms` flag was reset.",
            "type": "list"
        }, {
            "name": "run_analyzer",
            "value": false,
            "desc": "Whether to run the analyzer GUI at the end of the simulation. This analyzer is based on MATLAB code.",
            "type": "bool"
        }
    ],
    
    "configs": [
        {
            "groupname": "deformation",
            "params": [
                {
                    "name": "deformation_type",
                    "value": "exponential",
                    "desc": "The type of deformation profile. Can be one of the following: (1) delta, (2) exponential, and (3) gaussian.",
                    "type": "string"
                }, {
                    "name": "deformation_center_loc",
                    "value": [10, 10],
                    "desc": ["The location in the mesh defined as the central point of the deformation applied. It is given as the x- and y-coordinate indices within the mesh.",
                            "Note: The indices are to be given as the cell number, and not as Python index, e.g. the first corner cell would be accessed via [1,1] and not [0,0]."],
                    "type": "list"
                }, {
                    "name": "deformation_amplitude",
                    "value": 0.5,
                    "desc": "The amplitude of the deformation applied at the central cell location.",
                    "type": "float"
                }, {
                    "name": "deformation_decay_dist",
                    "value": 2,
                    "desc": "When relevant, this indicate the decay distance (in cell numbers) according to which the deformation vanishes.",
                    "type": "integer"
                }, {
                    "name": "release_deformation",
                    "value": false,
                    "desc": "Whether to execute deformation release during the simulation.",
                    "type": "boolean"
                }, {
                    "name": "deformation_release_type",
                    "value": null,
                    "desc": "The type of deformation release profile. Can be one of the following: (1) delta, (2) linear, (3) exponential and (4) concave_parabolic.",
                    "type": "string"
                }, {
                    "name": "end_release_factor_wrt_to_max",
                    "value": 0.1,
                    "desc": "The deformation value at the end of the release, expressed as a fraction of the deformation amplitude apllied during the loading phase.",
                    "type": "float"
                }, {
                    "name": "loading_duration",
                    "value": 30.0,
                    "desc": "The duration of the loading phase (for each loading-unloading cycle). Expressed in time (seconds).",
                    "type": "float"
                }, {
                    "name": "release_duration",
                    "value": 40.0,
                    "desc": "The time required to release the deformation completely (until reaching the `end_release_factor_wrt_to_max`).",
                    "type": "float"
                }, {
                    "name": "deformation_cycle_length",
                    "value": 100.0,
                    "desc": "The duration of each cycle (loading + unloading) in a cyclicly applied deformation simulation.",
                    "type": "float"
                }, {
                    "name": "load_deformation",
                    "value": false,
                    "desc": "Whether to load an external deformation.",
                    "type": "bool"
                }, {
                    "name": "deformation_file_path",
                    "value": "benchmarks/indentor",
                    "desc": "The path to a .npy file containing a deformation to load.",
                    "type": "string"
                }
            ]
        }, {
            "groupname": "weight_function",
            "params": [
                {
                    "name": "filter_type",
                    "value": "gaussian",
                    "desc": "The type of filter to use as a spatial weight function. Can be either: (1) delta, (2) gaussian, (3) linear, (4) lorenzian, (5) polynomial, and (6) sigmoid.",
                    "type": "string"
                }, {
                    "name": "filter_amplitude",
                    "value": 1.0,
                    "desc": "The amplitude at the central location of the spatial filter.",
                    "type": "float"
                }, {
                    "name": "gaussian_variance",
                    "value": 1.0,
                    "desc": "The variance of the Gaussian spatial filter.",
                    "type": "float"
                }, {
                    "name": "linear_inclination",
                    "value": 2.5,
                    "desc": "For the linear filter, this dictates the inclination (until reaching zero).",
                    "type": "float"
                }, {
                    "name": "lor_gamma",
                    "value": 1.0,
                    "desc": "The gamma parameter of the lorenzian filter.",
                    "type": "float"
                }, {
                    "name": "poly_power",
                    "value": 2.0,
                    "desc": "The power value of the polynomial filter, defined as: 1 / ([r + 1]^poly_power).",
                    "type": "float"
                }, {
                    "name": "sigm_beta",
                    "value": 0.5,
                    "desc": "The beta parameter of the sigmoid filter, defined as: 1 / (1 + exp(beta + [r - sigm_center])).",
                    "type": "float"
                }, {
                    "name": "sigm_center",
                    "value": 1.5,
                    "desc": "The rc parameter of the sigmoid filter, as defined in the previous flag.",
                    "type": "float"
                }
            ]
        }
    ]
}