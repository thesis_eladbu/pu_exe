# Pressure Ulcer Modeling

This repository contains a simulation for the mathematical model describing the dynamic development of pressure ulcers (PUs) in deep tissue muscles.
The model incorporates a spectrum of key processes that contribute to the injury, incorporating both cell level phenomena, e.g. deformation-induced changes in cell membrance permeability, and also tissue level processes, e.g. impaired diffusion based on species molecular weight.

## Table of Contents
1. [Installation](#installation)
2. [Usage](#usage)
3. [Project Structure](#project-structure)
4. [Simulation flags](#simulation-flags)

## Installation

Currently supported OS is Windows.

To start using the project, first of all clone the repository into a local directory:
```bash
$ cd <path/to/local/directory>
$ git clone https://gitlab.com/thesis_eladbu/pu_exe.git
```

## Usage

To run the application with default configuration, make sure you are based at the project's root directory (/pu_exe) and then run:

```bash
$ cd <path/to/root/directory>
$ dist/main
```

To run it with different configuration flags, the user can pass explicitly different flags to the main function alongside their desired values. For example, to change the maximal time of the simulation and the deformation type applied to the tissue to Gaussian, run the following:

```bash
$ dist/main --max_sim_time=260 --deformation_type=gaussian
```

The code would set these specificed attributes as desired, while loading the default values for the rest of the flags (not specified by the user). A list of all the flags can be found in the JSON file pu_config.json.
There is also another possibility, when the user wishes to udpate a large set of parameters and/or to save a configuration for future purposes, to pass the main function a path to a pre-defined configuration file. This file is expected in the JSON format, and must be one-level-deep, e.g. saving the following as example_config.json:

```python
{
	"mesh_size": [19, 19],
	"max_sim_time": 360,
	
	"deformation_type": "exponential",
	"deformation_center_loc": [10, 10],
	"deformation_amplitude": 0.5,
	"deformation_decay_dist": 2,

	"release_deformation": true,
	"deformation_release_type": "delta"
	"loading_duration": 30
}
```

Once it is defined, the user can pass it to the main function as follows:

```bash
$ dist/main --config_path=example_config.json
```

## Project Structure

The repository contains the following folders:
* __dist__: containing the project's main executable file.
* __example\_configs__: the folder contains example configuration (.json) files that can be fed into the simulation. These include for example IRI integrated model with various deformation release profiles. 
* __benchmarks__: like the previous folder, contains configurations, but also well defined and specific deformation profiles. These are based on experimental data.
	- __sigmoid__: the directory contains relevant data for running the simulation to re-create the single-step decreasing sigmoid describing the inverse relation between compression duration and external strain. For this purpose, run the application with the flag __build\_sigmoid__ set as true.

## Simulation flags

All of the simulation's flags are defined in the __pu\_config.json__ file under the project's root directory. This configuration file divides the flags into sub-categories for better readability:

* __input\_params__: general purpose flags, setting the simulation's type and profile, such as number of iterations to run, mesh size and mechanisms to include.
* __configs__: a list of configurable flag groups, including:
	- __deformation__: flags that define the type of deformation applied, its location in the mesh, whether to release it and how, etc.
	- __weight\_function__: a group of flags dictating the type and size of the tissue's weight function, with which the micro- and macro-scales are linked.

Each specific flag comes with its own description, along with a specification of its default value and type.
